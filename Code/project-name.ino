const int redPin = 9;    
const int greenPin = 10;  
const int bluePin = 11;   

void setup() {
  
  Serial.begin(9600);

  //OUTPUT
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
  
  // OFF LED
  digitalWrite(redPin, LOW);
  digitalWrite(greenPin, LOW);
  digitalWrite(bluePin, LOW);
}

void loop() {
     //
  if (Serial.available() > 0) {
    // ອ່ານຂໍ້ມູນຈາກ Serial
    String input = Serial.readStringUntil('\n'); 

    
    input.trim();

    //  OFF LED 
    digitalWrite(redPin, LOW);
    digitalWrite(greenPin, LOW);
    digitalWrite(bluePin, LOW);
    
    // OPEN LED ຕາມຄໍາສັ່ງ
    if (input == "r") { // ສີແດງ
      digitalWrite(redPin, HIGH);
    } else if (input == "g") { // ສີຂຽວ
      digitalWrite(greenPin, HIGH);
    } else if (input == "b") { // ສີຟ້າ
      digitalWrite(bluePin, HIGH);
    } else if (input == "on") { // ສີຂາວ
      digitalWrite(redPin, HIGH);
      digitalWrite(greenPin, HIGH);
      digitalWrite(bluePin, HIGH);
    } else if (input == "off") { // ປິດ
      
    } else {
      
    }
  }
}